import {useState, useEffect, useContext} from 'react';
import {ValueContext} from '../contexts/ValueContext';

export const useAPI = (API) =>{
    const {valueSearch, valueOrderedBy} = useContext(ValueContext)
    const [bid, setBid] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(()=>{
        setLoading(true)
        fetch(API
        //     {
        //     headers:{
        //         'Authorization':'Bearer keyeP0LnNpc4uz9Dk'
        //     }
        // }
        )
        .then(response => response.json() )
        .then(data => {
            setLoading(false)
            setBid(data.bids)
        })
    },[valueSearch])

    return {bid, loading}
}