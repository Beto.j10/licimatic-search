import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
        html {
                box-sizing: border-box;
                font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
        }
        
        *, *::before, *::after {
                box-sizing: inherit;
        }
        
        ul, li, h1, h2, h3, p, button {
                margin: 0;
        }

        ul {
                list-style: none;
        }

        button {
                background: transparent;
                border: 0;
                outline: 0;
        }

        body {
                background: #fefefe;
                /* background: rgba(37,94,255, .9); */
                /* background: #25B3FF; */
                height: 100vh;
                margin: 0 auto;
                max-width: 100vw;
                overscroll-behavior: none;
                width: 100%;
                font-size:16px;
        }

        #root {
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                overflow-x: hidden;
                min-height: 100vh;
                /* max-height: 99vh; */
                padding-bottom: 10px;
        }
        `