import React,{useContext} from 'react';
import {Header} from './components/header/Header';
import {UserSearch} from './components/userSearch/UserSearch';
import {GlobalStyle} from './GlobalStyles';
import {BodyWrap} from './components/bodyWrap/BodyWrap';
import {Hero} from './components/hero/Hero';
import {BodyCards} from './components/bodyCards/BodyCards';
import ValueSearch from './contexts/ValueContext';


function App() {
  
  return (
    <>
    <GlobalStyle/>
    <BodyWrap>
      <Hero>
        <Header/>
      </Hero>
      <ValueSearch>
        <UserSearch/>
        <BodyCards/>
      </ValueSearch>
    </BodyWrap>
    </>
  );
}

export default App;
