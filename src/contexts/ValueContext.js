import React,{createContext,useState} from 'react';

export const ValueContext = createContext({
    valueSearch:"",
    setValueSearch:() => null,
    valueWord:"",
    setValueWord:() => null,
    valueFilterstart:"",
    setValueFilterstart:() => null,
    valueFilterend:"",
    setValueFilterend:() => null,
    valueOrderedBy:"",
    setValueOrderedBy:() => null,
})

const ValueSearch = ({children}) =>{

    const [valueSearch, setValueSearch] = useState("")
    const [valueWord, setValueWord] = useState("")
    const [valueFilterstart, setValueFilterstart] = useState("")
    const [valueFilterend, setValueFilterend] = useState("")
    const [valueOrderedBy, setValueOrderedBy] = useState("")

    return(
        <ValueContext.Provider value={{
            valueSearch,
            setValueSearch,
            valueWord,
            setValueWord,
            valueFilterstart,
            setValueFilterstart,
            valueFilterend,
            setValueFilterend,
            valueOrderedBy,
            setValueOrderedBy
            }}>
            {children}
        </ValueContext.Provider>
    )

}
export default ValueSearch