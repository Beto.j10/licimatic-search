import styled from 'styled-components';     

export const SearchNotFound = styled.h3 `
    margin:2em 0 0;
    color: #0866F6;
    text-align: center;
`

export const Results = styled.h4 `
    width: 85%;
    margin:.5em auto;
    color: #0866F6;
    text-align: left;
    @media screen and (max-width:767px){
        width: 95%;
    }
`