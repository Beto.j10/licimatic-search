import React,{useContext} from 'react';
import {Cards} from '../cards/Cards';
import {useAPI} from '../../hooks/useAPI';
import {ValueContext} from '../../contexts/ValueContext';
import {SearchNotFound, Results} from './styles';
import {Loading} from '../loading/Loading';


export const BodyCards = () =>{
    const {valueWord, valueSearch, valueFilterstart, valueFilterend, valueOrderedBy} = useContext(ValueContext)

    const API =`https://buscadorlici.de/search?${valueSearch}`
    // const API =`http://127.0.0.1:8080/search?${valueSearch}`
    // const API =`http://127.0.0.1:8080/search/valledu`
    const {bid, loading }= useAPI(API)
    if (loading) return <Loading/>
    if (bid === null) {
        var hidden = true;
    }else{
        if (bid.length < 2) {

            var bidLength = true
        }else{
            var bidLength = false
        }
    }
    if (valueWord === "allbidslicimatic") {
        var results = true
    }else{
        var results = false
    }
    const formatterPeso = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0
      })
    var amountStart = formatterPeso.format(valueFilterstart)
    var amountEnd = formatterPeso.format(valueFilterend)
    // if (valueFilterstart !="") {
    //     var start = true
    // }else{
    //     start = false
    // }
    // if (valueFilterend != "") {
    //     var end = true
    // }else{
    //     var end = false
    // }
    
    return(
        <>
        {/* {(amountStart !== 0 || amountEnd != 0) && <Results>{amountStart} - {amountEnd}</Results> } */}
        {
            !hidden &&
            (bidLength ?
            (results ? <Results>{bid.length} Resultados:</Results> :
            <Results>{bid.length} Resultado para "{valueWord}":</Results>):
            (results ? <Results>{bid.length} Resultados:</Results> :
            <Results>{bid.length} Resultados para "{valueWord}":</Results>))
        }
        {
            hidden ?
            (results ?
            <SearchNotFound >Sin resultados </SearchNotFound> :
            <SearchNotFound >Sin resultados para "{valueWord}" </SearchNotFound> ):
            bid.map(data =>
                <Cards key={data.ID} {...data} />
                )
        }
        </>
    )
}
