import React from 'react';
import {HeroStyle} from './styles';

export const Hero = ({children}) =>{

    return(
        <HeroStyle>
            {children}
        </HeroStyle>
    )
}