import React from 'react';
import {Wrap, TitleCard, DescriptionCard, LinkCard, ValorCard, DetailsCard, DateCard, BidMoney, BidDate, BidPlace, BidUrl} from './styles';
import {Bill, Calendar, Place, Url} from '../icons/svg';

export const Cards = ({EntityName, Price, Title, Description, URL, CreatedAt}) => {
    const formatterPeso = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0
      })

      var date = new Date(CreatedAt);
      var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};

      date = date.toLocaleDateString("es-ES", options);
    return(
        <>
        <Wrap>
            <TitleCard> {Title} </TitleCard>
            <BidPlace>
                <Place/>
                <DetailsCard> {EntityName} </DetailsCard>
            </BidPlace>
            <DescriptionCard> {Description} </DescriptionCard>
            <BidMoney>
                <Bill/>
                <ValorCard>{formatterPeso.format(Price)}</ValorCard>
            </BidMoney>
            <BidDate>
                <Calendar/>
                <DateCard>Publicada {date}</DateCard>
            </BidDate>
            <BidUrl>
                <Url/>
                <LinkCard href={URL} target="_blank">{URL}</LinkCard>
            </BidUrl>
        </Wrap>
        </>
    )
}