
import styled from 'styled-components';

export const Wrap = styled.div`
    box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.1);
    width: 85%;
    color: #222222;
    margin: 1.2em auto;
    /* border: 1px solid grey; */
    border-radius: .4em;
    padding: 1.5em 2em;
    overflow:hidden;
    @media screen and (max-width:767px){
        width: 95%;
      padding: 1em 1em;

    }
    
`
export const TitleCard = styled.p`
    color: #222222;
    margin: .5em 0;
    font-weight: 600;

`
export const DescriptionCard = styled.p`
    color: #595959;
    margin: .5em 0;


`
export const LinkCard = styled.a`
    margin: .5em .5em;


`
export const ValorCard = styled.p`
    color: #222222;
    font-weight: 600;
    margin: .5em .5em;


`
export const DetailsCard = styled.p`
    color: #222222;
    margin: .5em .5em;


`
export const DateCard = styled.p`
    color: #222222;
    margin: .5em .5em;
    font-weight: 400;

`
export const BidMoney = styled.div`
  display:flex;
  /* justify-content:center; */
  align-items:center;
  svg {
  }

`
export const BidDate = styled.div`
  display:flex;
  /* justify-content:center; */
  align-items:center;
  svg {
  }

`
export const BidPlace = styled.div`
  display:flex;
  /* justify-content:center; */
  align-items:center;
  svg {
  }

`
export const BidUrl = styled.div`
  display:inline-flex;
  /* justify-content:center; */
  align-items:center;
  svg {
  }

`