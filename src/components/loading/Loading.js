import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import {LoadingStyle} from './styles';


export const Loading = () =>{
    return(
        <LoadingStyle>
            <CircularProgress />
        </LoadingStyle>

    )
}