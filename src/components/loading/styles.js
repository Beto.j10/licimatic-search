import styled from 'styled-components';

export const LoadingStyle = styled.div `
    margin: 0 auto;
    padding: 3em;
    text-align:center;
`