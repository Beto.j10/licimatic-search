import React from 'react';
import logo from '../../assets/static/licimatic.png';
import {HeaderWrap, TitleHero, LinkLicimatic} from './styles';

export const Header = () =>{

    return(
        <>
        <HeaderWrap>
            <img src={logo} width='280px' alt='LiciMatic'/>
        </HeaderWrap>
        <LinkLicimatic href="https://www.licimatic.com/" target="_blank">😃 Prueba 7 días gratis</LinkLicimatic>
        <TitleHero>Licitaciones publicadas hoy en Colombia</TitleHero>
        </>
    )
}