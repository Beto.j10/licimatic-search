import styled from 'styled-components';

export const HeaderWrap = styled.div `
    margin: 0 auto;
    padding:1.2em 1.2em;
    background: #fff;
    width: fit-content;
    height:fit-content;
`

export const TitleHero = styled.h1 `
    
    font-weight: 500;
    margin: .5em auto 0;
    font-size: 2.8em;
    color: #fff;
    @media screen and (max-width:767px){
    }
`
export const LinkLicimatic = styled.a`
    width: fit-content;
    display: flex;
    position:relative;
    text-decoration: none;
    font-weight:400;
    color: #fff;
    margin:1.5em auto 0;
    :hover {
        font-size: 1.1em;
    }
 
`