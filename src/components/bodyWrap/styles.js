import React from 'react';

import styled from 'styled-components';

export const BodyStyle = styled.div`

    /* top: 1em;
    position:relative;
    background: #fefefe;
    height: height;
    margin: 0 auto;
    max-width: 90vw;
    box-shadow: 0 0 15px rgba(0, 0, 0, .8);
    width: 100%;
    border-radius:1em;
    padding:0 0 1em; */
    font-size: 16px;
    @media screen and (max-width: 767px){
        font-size: .8em;
    }
`