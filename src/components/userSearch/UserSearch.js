import React,{useContext, useEffect, useState} from 'react';
import {Input, TitleSearch, Searcher, InputAmount, ButtonSearch, ButtonReset, Select, BidFilter, BidFilterDiv} from './styles';
import {ValueContext} from '../../contexts/ValueContext';
import {Filter} from '../icons/svg';

export const UserSearch = () =>{

    const [money, setMoney] = useState(0)
    
    const formatterPeso = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0
      })


    let search = ""
    const {
        valueWord, 
        setValueWord, 
        valueSearch, 
        setValueSearch,
        valueFilterstart,
        setValueFilterstart,
        valueFilterend,
        setValueFilterend,
        valueOrderedBy,    
        setValueOrderedBy,
    } = useContext(ValueContext)


    // useEffect(()=>{
    //     search = `word=${valueWord}&filterstart=${valueFilterstart}&filterend=${valueFilterend}&orderedBy=${valueOrderedBy}`
    //     setValueSearch(search)
    // },[valueOrderedBy])


    // const OnKeyPress = (e) =>{
    //     if (e.key === "Enter") {
    //         setValueWord(e.target.value)
    //     }
    // }

    const OnSubmit = (e) =>{
        
        e.preventDefault();
        setValueWord(e.target.inputWord.value)
        setValueFilterstart(e.target.inputInitial.value)
        setValueFilterend(e.target.inputEnd.value)
        setValueOrderedBy(e.target.selectedOrder.value)

        search = `word=${e.target.inputWord.value}&filterstart=${e.target.inputInitial.value}&filterend=${e.target.inputEnd.value}&orderedBy=${e.target.selectedOrder.value}`
        // search = `word=${e.target.inputWord.value}&filterstart=${e.target.inputInitial.value}&filterend=${e.target.inputEnd.value}`
        setValueSearch(search)
    }

    // onChange={e => setValueSearch(e.target.value) }
    return(
        <Searcher>
            <form onSubmit={OnSubmit} >
                <BidFilterDiv>
                    <BidFilter>
                        <Filter/>
                    </BidFilter>
                    <Select name='selectedOrder'>
                        <option value="recentDate">Más reciente</option>
                        <option value="oldDate">Más antigua</option>
                        <option value="higherValue">Mayor valor</option>
                        <option value="lowerValue">Menor valor</option>
                    </Select>
                </BidFilterDiv>
            <TitleSearch>Buscar Licitación</TitleSearch>
                <InputAmount name='inputInitial' placeholder='Valor inicial' type='number' />
                <InputAmount name='inputEnd' placeholder='Valor final' type='number'/>
                {/* <Input name='inputWord' placeholder='Busqueda por palabra' type='search' onKeyPress={OnKeyPress} /> */}
                <Input name='inputWord' placeholder='Busqueda por palabra' type='search' />
                <ButtonReset type='reset' >Limpiar</ButtonReset>
                <ButtonSearch type='submit' >Buscar</ButtonSearch>
            </form>
            <div>
            </div>
        </Searcher>
    )
}