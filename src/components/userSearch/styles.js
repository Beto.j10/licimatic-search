import styled from 'styled-components';

export const Input = styled.input`
    /* background: #DCE3F1; */
    display:block;
    color: #222222;
    background: #fff;
    font-size: 1em;
    /* border: 1px solid #ccc; */
    border: 1px solid rgba(8,102,246,1);
    box-shadow: 0px 0px 0px 1px rgba(8,102,246,.1);
    width: 60%;
    border-radius:.3em;
    padding: .2em 1em;
    margin: .3em auto;
    /* box-shadow: 0px 0px 1px 1px rgba(58,109,255,7); */
    height: 2em;


    :focus{
        outline: none;
        box-shadow: 0px 0px 6px 1px rgba(8,102,246,.5);

        
    }
    @media screen and (max-width:767px){
        width: 85%;
        font-size:1.2em;
    }
`
export const InputAmount = styled(Input)`
    /* background: #DCE3F1; */
    display: inline-flex;
    width: 13%;
    margin: .2em .3em .1em;
    height: 1.5em;
    border: 1px solid rgba(8,102,246,.2);
    :focus{
        border: 1px solid rgba(8,102,246,.8);
        box-shadow: 0px 0px 4px 2px rgba(8,102,246,.2);
    }
    @media screen and (max-width:767px){
        width: 40%;
    }
`
export const Select = styled.select`
    margin:1em auto .2em;
    font-size:1em;
    color: #404040;
    outline: none;
    border: 1px solid rgba(8,102,246,.5);
    box-shadow: 0px 0px 0px 1px rgba(8,102,246,.1);
    border-radius:.2em;
    background: #FEFEFE;
    padding: .1em;

    :hover{
        cursor:pointer;
        color: rgba(0,0,0,.8);
        outline: none;
        border: 1px solid rgba(8,102,246,.8);
        box-shadow: 0px 0px 6px 1px rgba(8,102,246,.5);
    }
    @media screen and (max-width:767px){
        font-size:1.1em;
    }
`

export const Submit = styled.input `

    border: 1px solid #ccc;
    color: #3D77F6;
    background: #E8F0FE;
    border-radius:.1em;
    margin: 0 .3em;
    font-size: 1em;


`

export const TitleSearch = styled.h2`
    /* color: #255EFF; */
    /* color: #585858; */
    color: #0866F6;
    font-weight:450;
    margin: 0 auto .1em;


`
export const Searcher = styled.span`

    display: block;
    text-align: center;
    background: #FEFEFE;
    padding: .5em 0 0;
    width: auto;
    margin: .5em auto;
    border-radius:.5em;
`

export const ButtonReset = styled.button`
    font-size:1em;
    color: #2177F6;
    border: 1px solid rgba(33,119,246,1);
    border-radius:.35em;
    padding: .5em 2em;
    margin: .5em;
    :hover{
        cursor:pointer;
        color: rgba(0,0,0,.8);
        background: rgba(0,0,0,.1)
    }
`
export const ButtonSearch = styled(ButtonReset)`
    background: #2177F6;
    color: #FEFEFE;
    :hover{
        color: #FFF;
        background: #1755B3;
    }
`
export const BidFilter = styled.div`
    position:relative;
    display:inline-flex;
    /* justify-content:center; */
    align-items:center;
    margin:0 .5em;
    bottom:0;
    svg {
    }

`
export const BidFilterDiv = styled.div`
    position:relative;
    display:block;
    /* justify-content:center; */
    align-items:center;
    margin:0 auto;


`